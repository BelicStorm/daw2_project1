const products_graphql_query = require("../services/graphql_services/products.querys");
const shops_graphql_query = require("../services/graphql_services/shops.querys");
class product_listCtrl {
    constructor(APIService,GraphqlService) {
        this.$onInit = function () {
            let ctrl = this
            /* Variables de tipo */
            ctrl.info;ctrl.shops;
            ctrl.favs;ctrl.products;
            ctrl.all_products;
            ctrl.wiki_eras;ctrl.wiki_sucesos;
            /* Variables de tipo */

            ctrl.APIService = APIService;
            ctrl.GraphqlService =GraphqlService;
            switch (ctrl.filter) {
                case "all":
                    ctrl.get("planets","all");
                    break;
                case "favorited":
                     ctrl.get("planets/get/favs","favs");
                    break;
                case "products":
                     ctrl.get_graphql(products_graphql_query.poducts_query.get_all,"products");
                    break;
                case "all_products":
                        ctrl.get_graphql(products_graphql_query.poducts_query.get_all,"all_products");
                    break;
                case "shops":
                     ctrl.get_graphql(shops_graphql_query.shops_query.get_all,"shops");
                    break;
                case "eras":
                        ctrl.get_moleculer("wiki_eras/eras","eras","starwars1");
                    break;
                case "sucesos":
                        ctrl.get_moleculer("wiki_sucesos/sucesos","sucesos","starwars2");
                    break;
                default:
                    break;
            }
        };
    }
    get(url,type){
        let ctrl = this
        /* console.log(url) */
        ctrl.APIService.get_delete(url, "GET").then(function (result) {
           switch (type) {
               case "all":
                ctrl.info = result;
                   break;
           
               case "favs":
                ctrl.favs = result;
               break
           }
        });
    }
    get_moleculer(url,type,domain){
        let ctrl = this
        /* console.log(url) */
        ctrl.APIService.get_from_moleculer(url, "GET",domain).then(function (result) {
           switch (type) {
               case "eras":
                    console.log(result)
                ctrl.wiki_eras = result;
               break
               case "sucesos":
                ctrl.wiki_sucesos = result;
               break
           }
        });
    }
    get_graphql(query,type){
        let ctrl = this;
        
        ctrl.GraphqlService.get(query).then(function (result) {
            switch (type) {
                case "products":
                    /* console.log(result) */
                    ctrl.products = result.all_products.slice().reverse();
                break;
                case "all_products":
                    /* console.log(result) */
                    ctrl.all_products = result.all_products;
                break;
                case "shops":
                    console.log(result)
                    ctrl.shops = result;
                break
            }
            return result;
        })
    }
}
let product_list = {
    bindings: { 'filter': '=' },
    controller: product_listCtrl,
    template:require('./product-list.html')
    
}

export default product_list;

