import angular from 'angular';

// Create the module where our functionality can attach to
let authModule = angular.module('app.auth', []);

// Include our UI-Router config settings
import AuhtConfig from './auth.config';
authModule.config(AuhtConfig);


// Controllers
import AuthCtrl from './auth.controller';
authModule.controller('AuthCtrl', AuthCtrl);
import SocialCtrl from './socialAuth.controller';
authModule.controller('SocialCtrl', SocialCtrl);


export default authModule;