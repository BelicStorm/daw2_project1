/* const Send = require("../../agent"); */
class ContactCtrl {
    constructor(APIService,ToastrService,$state) {
        console.log("Contact Controller Ok");
        this.usuario = {nombre:"",apellido:"",email:"",queja:"",type:"normal_user"};
        this.APIService=APIService;
        this.ToastrService=ToastrService;
        this._$state = $state;
    }
    
    enviar(){
        let ctrl = this;
        let data=ctrl.usuario;

        ctrl.APIService.post_put("contact",data,"POST").then(function(result) {
            if (result.err ==undefined) {
                ctrl.ToastrService.create({option:'success',message:"Enviado con exito"});
                return true;
               /*  ctrl._$state.go('app.home'); */
            }else{
                console.log(result);
                ctrl.errors = {Error:{Error:': '+result.err}};
                return false;
            }
        }); 
    }
  }
  
  
  export default ContactCtrl;