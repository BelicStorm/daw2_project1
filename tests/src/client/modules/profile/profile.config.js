function ProfileConfig($stateProvider,$qProvider) {
  
    $stateProvider
    .state('app.profile', {
        url: '/profile',
        controller: 'ProfileCtrl as $ctrl',
        template: require('./profile.html'),
        resolve: {
          profile: function(UserService) {
            return UserService.ensureAuthIs(true)
          }
        }
      })
  };
  
  export default ProfileConfig;