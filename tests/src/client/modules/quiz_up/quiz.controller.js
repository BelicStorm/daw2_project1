const quiz = require("../../services/graphql_services/prisma_server_querys");
class QuizCtrl {
    constructor(UserService,ToastrService,GraphqlService,$state) {
        this.UserService = UserService;
        this.GraphqlService = GraphqlService;
        this.ToastrService = ToastrService;
        this.loged_user_data = UserService.quizUser();
        this.question;
        this.win;
        if(this.loged_user_data){
            console.log(this.loged_user_data)
            this.get_question(this.loged_user_data.id)
        }else{
            this.auth_controller = true;
            this.user_quiz_data = {register:{mail:"",name:"",loged_id:"0"},login:{mail:"",name:"",loged_id:"0"}}
            if(this.user_data=UserService.current){
                this.user_quiz_data.register = {mail:this.user_data.email,name:this.user_data.username,loged_id:this.user_data.email}
                this.user_quiz_data.login = {mail:this.user_data.email,name:this.user_data.username,loged_id:this.user_data.email}
            }
        }
        

    }
    async get_question(player_id){
        let ctrl = this;
        await ctrl.GraphqlService.mutation_prisma(quiz.prisma_server_querys.initGame({player:player_id})).then(function(res) {
            if(res.initGame!=null){
                console.log(res)
                ctrl.question = res
            }else{
                ctrl.win = true
            }
        })
    }
    async login(){
        let ctrl = this;
        await ctrl.UserService.login_quiz_user(quiz.prisma_server_querys.login(ctrl.user_quiz_data.login)).then(function(res) {
            console.log(res)
            if(res){
                window.location.reload();
            }else{
                ctrl.ToastrService.create({option:"error",message:"The user doesnt exist."})
            }
        })
    }
    async logOut(){
        let ctrl = this;
        await ctrl.UserService.logOut_quiz_user(); 
    }
    async register(){
        let ctrl = this;
        await ctrl.UserService.register_quiz_user(quiz.prisma_server_querys.register(ctrl.user_quiz_data.register)).then(function(res) {
            console.log(res)
            if(res){
                window.location.reload();
            }else{
                ctrl.ToastrService.create({option:"error",message:"The user already exist."})
            }
        })
    }
    async answer(answer){
        let ctrl = this;
        if (answer !=ctrl.question.initGame.question[0].questionId) {
            ctrl.ToastrService.create({option:"error",message:"Wrong Answer."})
            setTimeout(() => {
                window.location.reload();
            }, 1000);
        }else{
            let id = ctrl.question.initGame.id;
            await ctrl.GraphqlService.mutation_prisma(quiz.prisma_server_querys.winGame(id)).then(function(res) {
                console.log(res)
                ctrl.ToastrService.create({option:"succes",message:"God Answer."})
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            })
        }
    }
    
  }
 
  export default QuizCtrl;