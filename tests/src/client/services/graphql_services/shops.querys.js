export const shops_query = {
    get_all: `{
        all_shops{
          id
          name
          image
        }
      }`,
      get_shop_by_id: function(id) {
       return `{
        shop(id: "${id}") {
          id
          name
          image
          stock {
            product_id
            name
            description
            image
          }
        }
       }`
      }
    
  };