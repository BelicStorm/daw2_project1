'use strict'

const ApiGateway = require('moleculer-web')

module.exports = {
  name: 'wiki_eras',
  mixins: [ApiGateway],

  // More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
  settings: {
    port: process.env.PORT || 3010,

    routes: [
      {
        path: '/wiki_eras',
        whitelist: [
          // Access to any actions in all services under "/api" URL
          '**'
        ],
        aliases: {
          'GET eras/:id': 'eras.get',
          'GET eras': 'eras.getAll',
        },
        cors: true,
      }
    ],
  }
}
