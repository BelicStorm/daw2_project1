async function get_params(context) {
  let params = {questions: [], answers:[],players:[]}
  await context.prisma.questionses().then(function (all_questions) {
    params.questions=all_questions
  });
  await context.prisma.players().then(function (all_players) {
    params.players=all_players
  });
  await context.prisma.answers().then(function (all_answers) {
    params.answers=all_answers
  });

  return params
}
async function random_with_exclusion(numbers) {
  var num = Math.floor(Math.random() * (numbers.max - numbers.min + 1)) + numbers.min;
  if(!numbers.no_repeat){
     do {
      num = Math.floor(Math.random() * (numbers.max - numbers.min + 1)) + numbers.min;
      played_games=await numbers.context.prisma.newGames({
        where:{result:"win" ,
        AND:{player_some:{id:numbers.player.id},
        AND:{
          question_some:{questionId:num}}}}
      })
     } while (played_games !=0);
   return num;
  }else{
    return (num === numbers.no_repeat) ? random_with_exclusion(numbers) : num;
  }
}
async function delete_all_playing_games(context,player) {
  await context.prisma.deleteManyNewGames({result_not:"win",
    AND:{player_some:{id:player.id}}})
  return 0
}
const Query = {
/* Queries to obtain all info */
  async all_questions(parent,args, context) {
     const  all_questions = await context.prisma.questionses();
    return  all_questions
  },
  async all_categories(parent,args, context) {
    const  all_categories = await context.prisma.categories();
   return  all_categories
 },
  async all_players(parent,args, context) {
    const  all_players = await context.prisma.players();
  return  all_players
  },
  async all_answers(parent,args, context) {
    const  all_answers = await context.prisma.answers();
  return  all_answers
  },
  async login(parent,{mail},context){
    console.log(mail)
    const params = await get_params(context);
    const found_player =  params.players.find(player_obj => player_obj.mail == mail);
/*     console.log(found_player) */
    return found_player;
  }
}

const Mutation = {
  async initGame(parent, { player }, context) {
    /* console.log(player) */
    const params = await get_params(context);
    const found_player =  params.players.find(player_obj => player_obj.id == player);
    await delete_all_playing_games(context,found_player);
    const wined_questions = await context.prisma.newGames({
      where:{result:"win",
      AND:{player_some:{id:player.id}}}
    })
    if(wined_questions.length != 6){
      const question_id = await random_with_exclusion({min:1,max:params.answers.length, context:context, player:found_player})
      const found_question = params.questions.find(question_obj => question_obj.questionId == question_id);
      const answer_id= await random_with_exclusion({min:1,max:params.answers.length,
                                          no_repeat:found_question.questionId})
      const found_answer2 = params.answers.find(answer_obj => answer_obj.answerId == answer_id);
      return context.prisma.createnewGame({
        player:{connect:{id:found_player.id}},
        question:{connect:{id:found_question.id}},
        answer2:{connect:{id:found_answer2.id}},
        result:"playing"
      })
    }
    
  },
  async update_game(parent, { game }, context){
    const games = await context.prisma.newGames()
    const found_games = games.find(game_obj => game_obj.id == game);
    return await context.prisma.updatenewGame({
      where:{id:found_games.id },
        data:{result:"win"}
    })
      
  },
  async delete_game(parent, { game }, context){
    const games = await context.prisma.newGames()
    const found_games = games.find(game_obj => game_obj.id == game);
    return await context.prisma.deleteManyNewGames({id:found_games.id})
      
  },
  async create_user(parent,{mail,name,loged_id},context){
    const params = await get_params(context);
    const found_player =  params.players.find(player_obj => player_obj.mail == mail);
    if(!found_player){
      if(loged_id !="0"){
        return context.prisma.createPlayer({
          mail:mail,
          name:name,
          loged_id:loged_id
        })
      }else{
        return context.prisma.createPlayer({
          mail:mail,
          name:`${name} (no_registered_user)`
        })
      }
    }
    
  }
}

module.exports = {
  Query,
  Mutation
}
