const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
const session = require('express-session');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');
const routes = require('./routes');
const app = express();


const host = process.env.API_HOST
const port = 3002;

app.use(session({ secret: 'Babalan Pasalan', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));

mongoose.connect('mongodb://localhost:27017/StarWarsTravel_test',function (err) {
    if (err) console.log('Not connected');
    console.log('Successfully connected');
});
 

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require('./config/passport');
app.use(passport.initialize());
app.use(passport.session());

app.use(require('./routes'));


const swaggerDocument = require('./swagger.json');
swaggerDocument.host=host + port;

app.use('/api_docs_test',swaggerUi.serve, swaggerUi.setup(swaggerDocument));

async function main() {
    try {
     
        await app.listen(port, () => {
            console.log(port)
            app.use(express.static('public')); //Print the index
        });
    } catch (e) {
        console.log(e)
    }
}
main();
