const mongoose = require('mongoose');
const router = require('express').Router();
const Planets = require('../../models/Planets');
const PlanetDb = mongoose.model("Planets");
const Users = require('../../models/Users');
let User = mongoose.model('User');
const Comments = require('../../models/Coments');
let Comment = mongoose.model('Comment');
let auth = require('../auth');

router.get('/', function(req,res,next) {
    PlanetDb.find().then(function(Planets){
        return res.json({data: Planets});
      }).catch(next);
});
//get by planet name
router.get('/:planet_name', function(req,res,next) {
    console.log(req.params.planet_name)
    let slug = req.params.planet_name
    PlanetDb.findOne({ slug: slug}).then(function(Planets){
        return res.json({data: Planets});
      }).catch(next);
});
//delete by planet name
router.delete('/:planet_name', function(req, res, next){
    let slug = req.params.planet_name
    PlanetDb.findOneAndDelete({slug:slug}).then(function(result){
       if(result!=null){res.json({data:"ok"})}else{res.json({data:"Planeta no encontrado"})}
    });
});
//create a planet
router.post('/', function(req, res, next){
    let slug = req.params.planet_name
    let info = req.body.data;
    let new_planet = new PlanetDb();
    new_planet.name=info.name;
    new_planet.rotation_period=info.rotation_period;
    new_planet.orbital_period=info.orbital_period;
    new_planet.diameter=info.diameter;
    new_planet.climate=info.climate;
    new_planet.gravity=info.gravity;
    new_planet.terrain=info.terrain;
    new_planet.surface_water=info.surface_water;
    new_planet.population=info.population;
    new_planet.image=info.image;

    new_planet.save().then(function(){
        return res.json({data: new_planet.toJSONFor()});
    }).catch(next);
});
//update a planet
router.put('/:planet_name',function(req, res, next){
    let slug = req.params.planet_name;
    let info = req.body.data;
    PlanetDb.findOneAndUpdate({slug:slug},info).then(function(result){
        if(result!=null){res.json({data:{response:"Actualizado",updated_data:info}})}
        else{res.json({data:"Planeta no encontrado o imposible de actualizar"})}
     }).catch(next);
});

/* Social actions */
// Favorite an planet
router.post('/:planet_name/favorite', auth.required, function(req, res, next) {
    console.log(req.params.planet_name) 
    PlanetDb.find({slug:req.params.planet_name}).then(function(planet) {
        let planet_id= planet[0]._id;
        User.findById(req.payload.id).then(function(user){
            if (!user) { return res.sendStatus(401); }
            if(!user.isFavorite(planet_id)){
                return user.favorite(planet_id).then(function(){
                    return planet[0].updateFavoriteCount().then(function(result){
                      return res.json({data: result.isFavorited(user)});
                    });
                  });
            }else{
                return user.unfavorite(planet_id).then(function(){
                    return planet[0].updateFavoriteCount().then(function(result){
                      return res.json({data: result.isFavorited(user)});
                    });
                  });
            }
            
          }).catch(next);
    })
});
router.get('/:planet_name/favorited',auth.required, function(req, res, next) {
  PlanetDb.find({slug:req.params.planet_name}).then(function(planet) {
    let planet_id= planet[0]._id;
      User.findById(req.payload.id).then(function(user){
          if (!user) { return res.sendStatus(401); }
          return res.json({data: planet[0].isFavorited(user)});
          
        }).catch(next);
    })
})
router.get('/get/favs',auth.required, function(req, res, next) { 
  User.findById(req.payload.id).then(function(user){
    PlanetDb.populate(user,{path:"favorites"}).then(function(favs){
      return res.json({data: favs});
    })
  })
})
//comments
// return an article's comments
router.get('/:planet_name/comments', auth.optional, function(req, res, next){
  Promise.resolve(req.payload ? User.findById(req.payload.id) : null).then(function(user){
      Comment.find({planet:req.params.planet_name})
      .populate({path:"author"})
      .then(function(comments){
        return res.json({data:comments})
      })
  })
  
});
router.get('/get_comment/:user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    Comment.find({author:user}).populate({path:"author"}).then(function(comments){
      return res.json({data:comments})
  })
 })
  
});
// create a new comment
router.post('/:planet_name/comments', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); } 
    let comment = new Comment(req.body.data);
    comment.planet = req.body.data.planet;
    console.log(comment.planet)
    comment.author = user;
    console.log(comment.toJSONFor())
    return comment.save().then(function(){
      return res.json({data:true});
    });
  }).catch(next);
});
router.delete('/:planet_name/comments/:comment', auth.required, function(req, res, next) {
  Comment.findByIdAndRemove(req.params.comment).then(function(){
    res.json({data:true})
  }).catch(next)
});
router.put('/:planet_name/comments/:comment', auth.required, function(req, res, next) {
  console.log(req)
  Comment.findByIdAndUpdate(req.params.comment,{ body: req.body.data}).then(function(){
    res.json({data:true})
  }).catch(next)
});

module.exports = router;